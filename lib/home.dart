import 'package:maul/appBar.dart';
import 'package:maul/colorPick.dart';
import 'package:flutter/material.dart';
import 'package:maul/viewHome.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        appBar: AllAppBar(),
        body: _grid(),
        backgroundColor: Colors.amber[50],
      ),
    );
  }

  Widget _grid() {
    return new Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: GridView.count(
        childAspectRatio: 8.0 / 9.0,
        shrinkWrap: true,
        mainAxisSpacing: 20,
        crossAxisCount: 2,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.grey,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://www.ykraya.com/wp-content/uploads/2020/02/sepatu-warriorsparta-hc-hitam-putih-ykraya.com-1-a-1.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Converse",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 600.499",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.grey,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://s3.bukalapak.com/img/3295480564/w-1000/Sepatu_Nike_Vapor_max_Original___Nike_Vapormax___Air_vapor_m.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Nike air max",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 15.000.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.grey,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://s2.bukalapak.com/img/7208277651/w-1000/Screenshot_2017_09_01_14_05_10_1.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Vans old school",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 700.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.grey,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/img/cache/700/product-1/2020/4/2/2877183/2877183_e4b75525-04f8-4f5d-bfd1-21707d379bfe_1108_1108.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Balenciaga",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 2.000.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.grey,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ph-test-11.slatic.net/p/ea04a4b9814087cac1d31f69a948af3d.jpg_720x720q75.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Converse low",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 400.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.grey,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://sneakernews.com/wp-content/uploads/2018/05/avengers-vans-old-skool.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Vans marvel",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 500.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
        ],
      ),
    );
  }
}
