import 'package:maul/about.dart';
import 'package:maul/colorPick.dart';
import 'package:maul/home.dart';
import 'package:maul/models/contact.dart';
import 'package:maul/produk.dart';
import 'package:flutter/material.dart';
import 'package:maul/ui/entryform.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';

  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [new BerandaPage(), EntryForm(), About()];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home,
            color: Colors.orangeAccent[100],
          ),
          icon: new Icon(
            Icons.home,
            color: Colors.grey,
          ),
          title: new Text(
            'Home',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.add_box_outlined,
            color: Colors.orangeAccent[100],
          ),
          icon: new Icon(
            Icons.add_box_outlined,
            color: Colors.grey,
          ),
          title: new Text('Tambah produk'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: Colors.orangeAccent[100],
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
      ],
    );
  }
}
